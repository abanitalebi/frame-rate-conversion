# Frame-Rate Conversion

## Introduction
This project implements an efficient frame-rate conversion algorithm 
for a stack of frames. The project aims to utilize state-of-the-art 
software and coding tools, but does not aim to use the state-of-the-art 
scientific algorithms for frame-rate conversion. It rather uses a simple
piece-wise cubic spline interpolation method.


## Tools Used
1. *Docker*
    
    1.1 Install Docker. Depending on your OS this would be different.

    1.2 To build a docker image from scratch follow instructions at 
`code_base/docker/dev`

    1.3 To pull and use an existing image follow instructions at
`code_base/docker/compose`

    1.4 The current docker setup includes a general purpose image that can
be used for many engineering and numerical programming practices.

2. *Python, C++, SWIG, multi-processing, opencv, numpy, etc* 


##  How-To / Setup
1. Follow instructions on how to build/pull a docker image

2. Create a container from your image

3. C++ bindings will be generated at the time of container creation

4. Run `docker exec -it mycontainer_ps_base_1 bash` 
to log into your container. Code is located at: 
`/opt/frame-rate-conversion`


## Demo

1. A demo is provided in 
`/opt/frame-rate-conversion/code_base/demo/demo.py`

2. This demo performs frame-rate up-conversion over a stack of input frames

3. Run the demo with: `python /opt/frame-rate-conversion/code_base/demo/demo.py`


## Comments and Questions:
Please do reach out to me with your suggestions.
 
*Email: dehkordi -at- ece -dot- ubc -dot- ca*

