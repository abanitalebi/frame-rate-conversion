#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR/code_base/fps_conversion/fruc_libs/cpp_lib/
rm -f _interpolator.so interpolator.py interpolator_wrap.c interpolator_wrap.cxx
swig -c++ -python interpolator.i
python setup.py build_ext --inplace

popd
