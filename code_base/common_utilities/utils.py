# Common utility functions

import os

import multiprocessing


def create_dir_if_needed(dir_path):
    """
    Creates a directory if needed, without raising any exceptions if the directory already exists.
    This handles race conditions where many processes try to create the same directory
    @param dir_path The directory path to create
    """

    if not os.path.isdir(dir_path):
        try:
            os.makedirs(dir_path)
        except OSError as exception:
            # Only directory exists is a valid exception
            if exception.errno != errno.EEXIST:
                raise


class NullContextManager:
    """
    No-op context manager that can be used in place of any other context manager
    """

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        return True


def get_write_lock(num_procs):
    """
    Creates and returns a write lock for multiprocessing. If num_procs is 1, then a null context manager is returned
    """

    if num_procs > 1:
        # Create lock for file I/O
        manager = multiprocessing.Manager()
        write_lock = manager.Lock()
    else:
        write_lock = NullContextManager()

    return write_lock
