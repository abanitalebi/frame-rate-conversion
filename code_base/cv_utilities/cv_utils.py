# Common Computer Vision utility functions, generally to handle image operations.

import cv2


def read_image(img_filepath):
    # Reads an image file and returns its data and dimensions
    img = cv2.imread(img_filepath)
    height, width, num_bands = img.shape

    return height, width, num_bands, img


def read_image_tile(frame_path, row, col, tile_height, tile_width):
    # Reads only a tile (rect) from an image file - for large images this should be faster than reading the whole thing
    # ToDo: Find a smart way of reading only the desired parts of an image, instead of reading the entire file
    img = cv2.imread(frame_path)
    aoi_data = img[row : row + tile_height, col : col + tile_width, :]

    return aoi_data


def write_image_tile(targ_filepath, aoi_data, row, col):
    # Writes only a tile (rect) to an image file
    # ToDo: Find a smart way of writing only the desired parts of an image, instead of the entire file
    img = cv2.imread(targ_filepath)
    aoi_shape = aoi_data.shape
    if len(aoi_shape) == 2:
        tile_height, tile_width = aoi_shape
    elif len(aoi_shape) == 3:
        tile_height, tile_width, num_bands = aoi_shape
    else:
        raise TypeError("Image AOI is expected to be either 2D or 3D but {}D shape was provided".format(len(aoi_shape)))

    img[row : row + tile_height, col : col + tile_width, :] = aoi_data
    cv2.imwrite(targ_filepath, img)

    return
