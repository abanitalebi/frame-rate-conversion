# Demo for frame-rate conversion.

import os

from code_base.frame_rate_converter_main import run_frame_rate_conversion

if __name__ == "__main__":
    input_dirpath = os.path.join(os.path.abspath(os.path.join(__file__, os.pardir)), 'input_frames')
    in_fps = 30
    out_fps = 60
    output_dirpath = os.path.join(os.path.abspath(os.path.join(__file__, os.pardir)), 'output_frames')
    num_procs = 2

    run_frame_rate_conversion(input_dirpath, in_fps, out_fps, output_dirpath, num_procs)
