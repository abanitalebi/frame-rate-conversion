# Managing Docker Containers

'docker/compose' package contains scripts needed to create docker containers from an existing
 docker image ('registry.gitlab.com/abanitalebi/frame-rate-conversion:latest')
 

## To create a container:
```*sh start_container.sh*```


## To stop a container:
```*sh stop_container.sh*```


## To remove a container:
```*sh remove_container.sh*```


## Notes:
1. Mounted directories can be set up within *docker-compose.yml* file.
2. In case you don't have access to pull the docker image from GitLab, you can
build an image by following the instructions in *docker/dev* package.


