#!/usr/bin/env bash

# USE WITH CAUTION
# This script removes all of the containers in the my_container project.
# If you want to stop your containers and continue working later, use the stop_container.sh script.
docker-compose -p my_container down