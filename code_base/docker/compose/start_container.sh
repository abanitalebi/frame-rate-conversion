#!/usr/bin/env bash

docker pull registry.gitlab.com/abanitalebi/frame-rate-conversion:latest
docker-compose -p mycontainer up -d
docker exec mycontainer_ps_base_1 /usr/bin/bash -c 'sh /opt/frame-rate-conversion/build_c_bindings.sh'
docker exec mycontainer_ps_base_1 /bin/sh -c "echo 'export PYTHONPATH=$PYTHONPATH:/opt/frame-rate-conversion/' >> ~/.bashrc"
docker exec mycontainer_ps_base_1 /bin/sh -c "echo 'export LD_LIBRARY_PATH=/usr/local/lib' >> ~/.bashrc"
