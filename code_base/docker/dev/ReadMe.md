# Build a docker image from scratch

This package contains scripts and source files to build a docker image
from scratch, from a fresh CentOS7 installation.


## To build an image from scratch:
```sh build_image.sh```

## To push this image to GitLab's docker repo:
```docker login registry.gitlab.com```

```docker build -t registry.gitlab.com/abanitalebi/frame-rate-conversion .```

```docker push registry.gitlab.com/abanitalebi/frame-rate-conversion```

## Notes:
1. To revise what packages you would like to include in the built image
you would have to modify *dockerfile*. The current dockerfile includes
 many packages needed for general engineering/numerical coding such as
 *python3.5, numpy, schipy, opencv, gdal, ffmpeg, java, cmake, pands, 
 ipython, ImageMagick, Emacs, vim, scikit-learn, Cython*, etc. Also, 
 pip packages can be updated via *requirements.txt*

 
2. If you don't have access to login/pull/push images to GitLab's repo,
you need to modify the *build_image.sh* as follows:

```arma.header
docker build -t frame-rate-conversion .
```
