#!/usr/bin/env bash

# To create an image locally from scratch:
# docker build -t frame-rate-converter .

# To create an image from scratch and push to GitLab's docker registry
docker login registry.gitlab.com
docker build -t registry.gitlab.com/abanitalebi/frame-rate-conversion .
docker push registry.gitlab.com/abanitalebi/frame-rate-conversion
