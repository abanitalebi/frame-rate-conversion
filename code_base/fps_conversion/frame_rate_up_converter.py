
import math
import multiprocessing
import os
import time

import cv2
import numpy as np
import psutil as psutil

from code_base.common_utilities.utils import create_dir_if_needed, get_write_lock
from code_base.cv_utilities.cv_utils import read_image, read_image_tile, write_image_tile
from code_base.fps_conversion.fruc_libs.cpp_interpolator_for_fruc import interpolate_time_series_cpp_lib

FRAME_FILENAME_PREFIX = 'frame_'
FRAME_FILENAME_SUFFIX = '_up_converted.tif'
DEFAULT_TILE_HEIGHT = 1000
DEFAULT_TILE_WIDTH = 1000
DEFAULT_FRAME_WINDOW_LENGTH = 6    # number of frames from all, within each multi-processing window (if available)
FRUC_MEM_USAGE_RATIO = 4 * 3 # Assuming each variable uses 4 times more memory in python, and a factor of 3 for other objects in memory, other than the frame_stack
MINIMUM_NUMBER_OF_FRAMES_REQUIRED_FOR_FRUC = 3
EPSILON = 1e-3

BYTES_TO_KB = 1024
KB_TO_MB = 1024
MB_TO_GB = 1024
BYTES_TO_MB = BYTES_TO_KB * KB_TO_MB
BYTES_TO_GB = BYTES_TO_MB * MB_TO_GB
MAX_MEM_GB = psutil.virtual_memory().total / BYTES_TO_GB


class FrameRateUpConverter:
    # Performs frame rate up-conversion

    def __init__(self, frame_file_paths, in_fps=10, out_fps=30, output_frames_folder_path='.',
                 output_frames_filename_prefix=FRAME_FILENAME_PREFIX,
                 output_frames_filename_suffix=FRAME_FILENAME_SUFFIX,
                 tile_height=None, tile_width=None, num_procs=1):

        # frame_file_paths: list containing paths to the input frames
        # Valid values: list of strings of local paths, e.g.:
        #     ["/scratch/[my-path]/frame_0000002.TIF", "/scratch/[my-path]/frame_0000003.TIF"]
        self.frame_file_paths = frame_file_paths
        self.num_input_frames = len(self.frame_file_paths)
        if self.num_input_frames < MINIMUM_NUMBER_OF_FRAMES_REQUIRED_FOR_FRUC:
            print("Frame Rate Up Conversion: Found less than {} frames - skipping the up-conversion".
                  format(MINIMUM_NUMBER_OF_FRAMES_REQUIRED_FOR_FRUC))
            return

        # input and output FPS
        self.in_fps = np.array(in_fps).astype('double')
        self.out_fps = np.array(out_fps).astype('double')

        # output_frames_folder_path: is a path to a folder, to which the output files will be written
        # valid values: a string pointing to the desired local path, e.g.: "/scratch/[my-dir]/fruc_outputs"
        create_dir_if_needed(output_frames_folder_path)
        self.output_frames_folder_path = output_frames_folder_path

        # output_frames_filename_prefix/suffix: a string prefix/suffix for the output frames
        # valid values: strings, e.g.: for prefix: "frame_" and for suffix: "_30_fps"
        # prefix will be added to the beginning of the output frame/mask names and suffix is added to the end
        self.output_frames_filename_prefix = output_frames_filename_prefix
        self.output_frames_filename_suffix = output_frames_filename_suffix

        # number of processes
        # valid values: natural values up to at most the maximum number of cores available
        self.num_procs = num_procs

        # tile_height/width: an integer value that specifies the height/width of a tile for multi-processing
        # valid values: integers, e.g. 200
        self.win_length = min(self.num_input_frames, DEFAULT_FRAME_WINDOW_LENGTH)
        tile_size_in_pixels = self.__calculate_tile_size_based_on_available_memory(self.win_length, self.out_fps, self.in_fps, self.num_procs)
        self.tile_height = tile_height if tile_height is not None else tile_size_in_pixels
        self.tile_width = tile_width if tile_width is not None else tile_size_in_pixels

    def __calculate_tile_size_based_on_available_memory(self, win_length, out_fps, in_fps, num_procs):
        # Calculates a rough estimate of the tile size using the available machine memory
        available_memory_in_bytes = MAX_MEM_GB / FRUC_MEM_USAGE_RATIO * BYTES_TO_GB # Use a portion of the available memory, to take into account memory handling in python
        tile_size_in_pixels = int(np.sqrt(available_memory_in_bytes / (win_length * out_fps / in_fps * num_procs * 3)))

        return tile_size_in_pixels

    def up_convert_frame_rate(self):

        in_fps = self.in_fps
        out_fps = self.out_fps
        frame_file_paths = self.frame_file_paths

        print("Frame Rate Up Conversion from {} fps to {} ...".format(in_fps, out_fps))
        start_time = time.time()

        # Handle trivial cases of in_fps == out_fps
        if abs(in_fps - out_fps) < EPSILON:
            print("Input frame-rate is identical or very close to the output frame-rate, no conversion will be done.")
            return frame_file_paths

        # Find the resolution of the frames
        number_of_frames = len(frame_file_paths)
        sample_frame_path = frame_file_paths[0]
        frame_height, frame_width, num_bands, _ = read_image(sample_frame_path)

        # Generate the list of output file paths
        output_frame_paths = []
        create_output_files_args_list = []
        number_of_output_frames = math.ceil((number_of_frames-1) * (out_fps/in_fps)+1)
        for frame_num in range(number_of_output_frames):
            targ_filepath = os.path.join(self.output_frames_folder_path, self.output_frames_filename_prefix + "%06d" % frame_num + self.output_frames_filename_suffix)
            output_frame_paths.append(targ_filepath)
            create_output_files_args_list.append([targ_filepath, frame_width, frame_height, num_bands])
        pool = multiprocessing.Pool(self.num_procs)
        pool.map(create_empty_output_file, create_output_files_args_list)

        # Setup write lock for multi-processing
        write_lock = get_write_lock(self.num_procs)

        # create vectors that contain the sampling time instances
        time_in = np.arange(0, in_fps+in_fps/(number_of_frames-1), in_fps/(number_of_frames-1))
        time_out = np.arange(0, in_fps, in_fps*in_fps/((number_of_frames-1)*out_fps))
        time_out = np.append(time_out, in_fps)

        # Perform interpolation for different tiles in parallel - within a moving window with overlap
        win_length = self.win_length
        fruc_tile_args = []
        for row in range(0, frame_height, self.tile_height):
            for col in range(0, frame_width, self.tile_width):

                window_counter = 0
                tile_height = self.tile_height if (row + self.tile_height < frame_height) else frame_height - row
                tile_width = self.tile_width if (col + self.tile_width < frame_width) else frame_width - col

                for frame_num in range(0, number_of_frames + len(range(0, number_of_frames, win_length)) + win_length, win_length):

                    frame_idx = frame_num if frame_num == 0 else frame_num - window_counter
                    if number_of_frames - frame_idx < win_length:  # interpolator needs at least a few frames for interpolation.
                        frame_idx = number_of_frames - win_length

                    time_in_in_window = time_in[frame_idx:frame_idx + win_length]
                    time_out_in_window = time_out[frame_idx * out_fps/in_fps:(frame_idx + win_length - 1) * out_fps/in_fps + 1]

                    number_of_output_frames_in_window = len(time_out_in_window)

                    frame_file_paths_in_window = frame_file_paths[frame_idx:frame_idx + win_length]
                    output_frame_paths_in_window = output_frame_paths[int(frame_idx * out_fps/in_fps):int((frame_idx + win_length - 1) * out_fps/in_fps + 1)]

                    fruc_tile_args.append([frame_file_paths_in_window, output_frame_paths_in_window, row, col, tile_height, tile_width, time_in_in_window,
                                           time_out_in_window, win_length, number_of_output_frames_in_window, num_bands, in_fps, out_fps, write_lock])

                    window_counter += 1

        pool = multiprocessing.Pool(self.num_procs)
        pool.map(_perform_tile_frame_rate_up_conversion, fruc_tile_args)

        print("Frame Rate Up Conversion: Finished in {} seconds".format(time.time() - start_time))

        return output_frame_paths


def _perform_tile_frame_rate_up_conversion(fruc_tile_args):
    """Performs standalone frame-rate up-conversion on a stack of frame-tiles. This function is called by
        multi processes for different tiles in parallel"""

    frame_file_paths, output_frame_paths, row, col, tile_height, tile_width, time_in, time_out, number_of_frames, \
    number_of_output_frames, num_bands, in_fps, out_fps, write_lock = fruc_tile_args

    print('FRUC: Processing tile lines: {} to {}, pixels: {} to {} for {} output frames'.format(
        row, row+tile_height, col, col+tile_width, number_of_output_frames))

    # Create a frame stack
    frame_stack = np.zeros((number_of_frames, tile_height, tile_width, num_bands))
    for frame_num, frame_path in enumerate(frame_file_paths):
        frame_stack[frame_num, :, :, :] = read_image_tile(frame_path, row, col, tile_height, tile_width)
    frame_stack = np.float64(frame_stack)

    # interpolate through the time to perform the actual up-conversion
    frame_stack_out = interpolate_time_series_for_tile(time_in, frame_stack, time_out, number_of_output_frames,
                                                       tile_height, tile_width, num_bands)

    # Cast the reconstructed frames to integers to be written to TIF files
    frame_stack_out = np.uint16(frame_stack_out)

    # Write the up-converted frames to disk
    for frame_num in range(number_of_output_frames):
        targ_filepath = output_frame_paths[frame_num]
        with write_lock:
            write_image_tile(targ_filepath, frame_stack_out[frame_num, :, :, :], row, col)


def interpolate_time_series_for_tile(time_in, frame_stack, time_out, number_of_output_frames,
                                     tile_height, tile_width, num_bands):

    frame_stack_out = interpolate_time_series_cpp_lib(time_in, frame_stack, time_out, number_of_output_frames,
                                                      tile_height, tile_width, num_bands)

    return frame_stack_out


def create_empty_output_file(args_list):
    # Creates empty files - called in parallel with multiple processes
    targ_filepath, frame_width, frame_height, num_bands = args_list
    initial_img = np.zeros((frame_height, frame_width, num_bands))
    cv2.imwrite(targ_filepath, initial_img)

    return
