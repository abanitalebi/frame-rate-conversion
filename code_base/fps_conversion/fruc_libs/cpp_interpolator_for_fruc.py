import numpy as np

from code_base.fps_conversion.fruc_libs.cpp_lib import interpolator


def interpolate_time_series_cpp_lib(time_in, frame_stack, time_out, num_frames_out, tile_height, tile_width, num_bands):
    """
    Performs interpolation over a stack of tiles - using a c++ interpolation function
    :param time_in: time stamps for input tile stack (e.g. [0, 1, 2, ..., 10])
    :param frame_stack: a 4-D array in the form of [num_frames, height, width, num_bands]
    :param time_out: output itme stamps for output tile stack (e.g. [0, 0.5, 1, 1.5, ..., 9.5, 10])
    :param num_frames_out: number of output frames
    :param tile_height: height
    :param tile_width: width
    :param num_bands: number of bands
    :return: output frame stack
    """

    frame_stack_arr_red = np.float32(frame_stack[:, :, :, 0].ravel())
    frame_stack_arr_grn = np.float32(frame_stack[:, :, :, 1].ravel())
    frame_stack_arr_blu = np.float32(frame_stack[:, :, :, 2].ravel())

    frame_stack_out = np.float32(np.zeros((num_frames_out, tile_height, tile_width, num_bands)))

    frame_stack_out_arr_red_length = tile_height * tile_width * num_frames_out
    frame_stack_out_arr_grn_length = tile_height * tile_width * num_frames_out
    frame_stack_out_arr_blu_length = tile_height * tile_width * num_frames_out

    frame_stack_out_arr_red, frame_stack_out_arr_grn, frame_stack_out_arr_blu = interpolator.interpolate_tile(
                                                                                  np.float32(time_in),
                                                                                  frame_stack_arr_red,
                                                                                  frame_stack_arr_grn,
                                                                                  frame_stack_arr_blu,
                                                                                  np.float32(time_out),
                                                                                  tile_height, tile_width, num_bands,
                                                                                  frame_stack_out_arr_red_length,
                                                                                  frame_stack_out_arr_grn_length,
                                                                                  frame_stack_out_arr_blu_length)

    frame_stack_out[:, :, :, 0] = np.reshape(frame_stack_out_arr_red, (num_frames_out, tile_height, tile_width))
    frame_stack_out[:, :, :, 1] = np.reshape(frame_stack_out_arr_grn, (num_frames_out, tile_height, tile_width))
    frame_stack_out[:, :, :, 2] = np.reshape(frame_stack_out_arr_blu, (num_frames_out, tile_height, tile_width))

    return frame_stack_out
