
// This is an interpolator in C++ that is used to create a binary library for frame-rate conversion interpolation.
// This library is much faster than its Python equivalent.

#include <algorithm>
#include <iostream>
#include <vector>

#include "spline.h"
#include "interpolator_utils.h"


// -------------------------------------------------------------------------------------
// Function to perform interpolation over a 1D vector
void piece_wise_cubic_interpolator(const std::vector<double>& x_in, const std::vector<double>& y_in,
                                   const float* x_out, const int x_out_arr_length, float* y_out,
                                   const unsigned int y_out_index_row, const unsigned int y_out_index_col,
                                   const unsigned int tile_height, const unsigned int tile_width)
{
    /* Fits a piece-wise cubic spline and return the values at the requested time stamps - operates over a 1-D vector */
    tk::spline my_spline;
    my_spline.set_points(x_in, y_in);
    for (int idx = 0; idx < x_out_arr_length; ++idx) {
        y_out[idx*tile_height*tile_width + y_out_index_row*tile_width + y_out_index_col] = std::max(1.0, my_spline(x_out[idx]));
    }
}


// -------------------------------------------------------------------------------------
/*
    Performs interpolation over a stack of tiles - using a c++ interpolation function
    @param time_in_arr_length: length of time_in_arr
    @param time_in_arr: 1D array that contains time stamps for input tile stack
    @param frame_stack_arr_red_length: length of frame_stack_arr_red
    @param frame_stack_arr_red: 1D array that contains input tile frame stack (red component) - reshaped from num_frames X height X width array
    @param frame_stack_arr_grn_length: length of frame_stack_arr_grn
    @param frame_stack_arr_grn: 1D array that contains input tile frame stack (grn component) - reshaped from num_frames X height X width array
    @param frame_stack_arr_blu_length: length of frame_stack_arr_blu
    @param frame_stack_arr_blu: 1D array that contains input tile frame stack (blu component) - reshaped from num_frames X height X width array
    @param time_out_arr_length: length of time_out_arr
    @param time_out_arr: output itme stamps for output tile stack
    @param tile_height: height
    @param tile_width: width
    @param num_bands: number of bands
    @param frame_stack_out_arr_red_length: length of frame_stack_out_arr_red
    @param frame_stack_out_arr_red: output tile frame stack as a 1D array (red component)
    @param frame_stack_out_arr_grn_length: length of frame_stack_out_arr_grn
    @param frame_stack_out_arr_grn: output tile frame stack as a 1D array (grn component)
    @param frame_stack_out_arr_blu_length: length of frame_stack_out_arr_blu
    @param frame_stack_out_arr_blu: output tile frame stack as a 1D array (blu component)
*/
void interpolate_tile(const unsigned int time_in_arr_length, float* time_in_arr,
                      const unsigned long long frame_stack_arr_red_length, float* frame_stack_arr_red,
                      const unsigned long long frame_stack_arr_grn_length, float* frame_stack_arr_grn,
                      const unsigned long long frame_stack_arr_blu_length, float* frame_stack_arr_blu,
                      const unsigned int time_out_arr_length, float* time_out_arr,
                      const unsigned int tile_height, const unsigned int tile_width,
                      const unsigned int num_bands,
                      const unsigned long long frame_stack_out_arr_red_length, float* frame_stack_out_arr_red,
                      const unsigned long long frame_stack_out_arr_grn_length, float* frame_stack_out_arr_grn,
                      const unsigned long long frame_stack_out_arr_blu_length, float* frame_stack_out_arr_blu)
{
    const unsigned int num_frames_in = time_in_arr_length;
    const unsigned int num_frames_out = time_out_arr_length;

    for (unsigned int row = 0; row < tile_height; ++row) {
        for (unsigned int col = 0; col < tile_width; ++col) {

            std::vector<double> pixel_stack_filtered_red, time_in_filtered_red;
            std::vector<double> pixel_stack_filtered_grn, time_in_filtered_grn;
            std::vector<double> pixel_stack_filtered_blu, time_in_filtered_blu;

            for (unsigned int frame_idx = 0; frame_idx < num_frames_in; ++frame_idx) {
                const double red_pixel_value = frame_stack_arr_red[frame_idx*tile_height*tile_width + row*tile_width + col];
                const double grn_pixel_value = frame_stack_arr_grn[frame_idx*tile_height*tile_width + row*tile_width + col];
                const double blu_pixel_value = frame_stack_arr_blu[frame_idx*tile_height*tile_width + row*tile_width + col];

                pixel_stack_filtered_red.push_back(red_pixel_value);
                time_in_filtered_red.push_back(time_in_arr[frame_idx]);

                pixel_stack_filtered_grn.push_back(grn_pixel_value);
                time_in_filtered_grn.push_back(time_in_arr[frame_idx]);

                pixel_stack_filtered_blu.push_back(blu_pixel_value);
                time_in_filtered_blu.push_back(time_in_arr[frame_idx]);
            }

            // Perform interpolation
            piece_wise_cubic_interpolator(time_in_filtered_red, pixel_stack_filtered_red, time_out_arr, num_frames_out,
                        frame_stack_out_arr_red, row, col, tile_height, tile_width);

            piece_wise_cubic_interpolator(time_in_filtered_grn, pixel_stack_filtered_grn, time_out_arr, num_frames_out,
                        frame_stack_out_arr_grn, row, col, tile_height, tile_width);

            piece_wise_cubic_interpolator(time_in_filtered_blu, pixel_stack_filtered_blu, time_out_arr, num_frames_out,
                        frame_stack_out_arr_blu, row, col, tile_height, tile_width);

        }
    }
}