
#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H


// Performs interpolation over a stack of tiles (arrays in 1D)
void interpolate_tile(unsigned int time_in_arr_length, float* time_in_arr,
                      unsigned long long frame_stack_arr_red_length, float* frame_stack_arr_red,
                      unsigned long long frame_stack_arr_grn_length, float* frame_stack_arr_grn,
                      unsigned long long frame_stack_arr_blu_length, float* frame_stack_arr_blu,
                      unsigned int time_out_arr_length, float* time_out_arr,
                      unsigned int tile_height, unsigned int tile_width,
                      unsigned int num_bands,
                      unsigned long long frame_stack_out_arr_red_length, float* frame_stack_out_arr_red,
                      unsigned long long frame_stack_out_arr_grn_length, float* frame_stack_out_arr_grn,
                      unsigned long long frame_stack_out_arr_blu_length, float* frame_stack_out_arr_blu);


#endif //INTERPOLATOR_H

