%module interpolator
%{
  #define SWIG_FILE_WITH_INIT
  #include "interpolator.h"
%}

%include "numpy.i"
%init %{
import_array();
%}


%apply (int DIM1, float* IN_ARRAY1) {(unsigned int time_in_arr_length, float* time_in_arr)};

%apply (int DIM1, float* IN_ARRAY1) {(unsigned long long frame_stack_arr_red_length, float* frame_stack_arr_red)};
%apply (int DIM1, float* IN_ARRAY1) {(unsigned long long frame_stack_arr_grn_length, float* frame_stack_arr_grn)};
%apply (int DIM1, float* IN_ARRAY1) {(unsigned long long frame_stack_arr_blu_length, float* frame_stack_arr_blu)};

%apply (int DIM1, float* IN_ARRAY1) {(unsigned int time_out_arr_length, float* time_out_arr)};

%apply (int DIM1, float* ARGOUT_ARRAY1) {(unsigned long long frame_stack_out_arr_red_length, float* frame_stack_out_arr_red)};
%apply (int DIM1, float* ARGOUT_ARRAY1) {(unsigned long long frame_stack_out_arr_grn_length, float* frame_stack_out_arr_grn)};
%apply (int DIM1, float* ARGOUT_ARRAY1) {(unsigned long long frame_stack_out_arr_blu_length, float* frame_stack_out_arr_blu)};

%include "interpolator.h"



/*
 Notes:

 The SWIG ".i" interface file, contains declarations that have to match the corresponding ".h" and ".py" files.

 For input arrays, in python call the array it self is passed, but in ".h" and ".i" two variables for each array are passed:
    array length, and array pointer. This is because SWIG has to know how much memory of what type (with what content)
    it has to allocate for each array.

 For output arrays, in python outputs are defined by out1, out2 = my_func(...). In".i" they are defined as "ARGOUT"
    type and use two variables:
    one for length and one pointer. In python, only one length variable (per output) is passed as an input variable, so
    SWIG knows how much memory of what type it needs to allocate. In '.h' and '.cc' outputs are defined in a void style (inputs that are modified).

 Scalars don't have to be included in the ".i" file as they are automatically passed. This is just a special case of
    anything that isn't mapped in the .i file is passed as-is to the function

 ".cpp"/".cc" need to match the ".h" file. Note that one ".i" file can define variables that are used in multiple
    function calls within a same ".h" and ".cc" file. Note that .h and .i must match exactly, including variable names.

 "numpy.i" contains numpy interface for SWIG. Documentation: https://docs.scipy.org/doc/numpy/reference/swig.interface-file.html
*/
