
#include <iostream>
#include <vector>


// Prints out the contents of a 1D double array - used for debugging
/*
    @param input_array: array to be printed out to the console
    @param array_length: length of input_array
*/
void print_1d_double_array(const double* input_array, const unsigned long array_length)
{
    std::cout << std::endl;
    for (unsigned long i = 0; i < array_length; i++) {
        std::cout << input_array[i] << "  ";
    }
    std::cout << std::endl;
}


// Prints out the contents of a 1D float array - used for debugging
/*
    @param input_array: array to be printed out to the console
    @param array_length: length of input_array
*/
void print_1d_float_array(const float* input_array, const unsigned long array_length)
{
    std::cout << std::endl;
    for (unsigned long i = 0; i < array_length; i++) {
        std::cout << input_array[i] << "  ";
    }
    std::cout << std::endl;
}


// Prints out the contents of a 1D double vector - used for debugging
/*
    @param input_vector: vector to be printed out to the console
*/
void print_1d_vector(const std::vector<double>& input_vector)
{
    std::cout << std::endl;
    for (unsigned int i = 0; i < input_vector.size(); i++) {
        std::cout << input_vector[i] << "  ";
    }
    std::cout << std::endl;
}


