
// Contains utility functions used for development of FRUC code.


#ifndef INTERPOLATOR_UTILS_H
#define INTERPOLATOR_UTILS_H


// Prints out the contents of a 1D double array - used for debugging
void print_1d_double_array(const double* input_array, const unsigned long array_length);


// Prints out the contents of a 1D float array - used for debugging
void print_1d_float_array(const float* input_array, const unsigned long array_length);


// Prints out the contents of a 1D double vector - used for debugging
void print_1d_vector(const std::vector<double> &input_vector);


#endif //INTERPOLATOR_UTILS_H