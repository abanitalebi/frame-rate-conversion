from distutils.core import setup, Extension
import numpy
import os

os.environ['CC'] = 'g++';
setup(name='uc_interpolator',
      version='1.0',
      ext_modules=[Extension('_interpolator',
                             ['interpolator_utils.cc',
                              'interpolator.cc',
                              'interpolator.i'],
                             include_dirs=[numpy.get_include(),'.'],
                             extra_compile_args=['-std=c++11'])]
      )