import argparse
import glob
import os

from code_base.fps_conversion.frame_rate_up_converter import FrameRateUpConverter

EPSILON = 1e-3


def run_frame_rate_conversion(input_dirpath, in_fps, out_fps, output_dirpath, num_procs=1):
    """

    :param input_dirpath: Path to directory that contains input frames - supports: tif, png, jpg, etc. Tested on color images.
    :param in_fps: input frame rate. Valid values: real positive e.g. 10.5 fps
    :param out_fps: desired output frame rate. Valid values: real positive, not necessarily greater than input fps
    :param output_dirpath: path to directory that contains output frames - will overwrite existing files
    :param num_procs: number of parallel processes to use
    :return: None
    """
    extensions = ['*.png', '*.jpg', '*.tif', '*.tiff', '*.bmp']
    video_frame_filepaths = []
    for ext in extensions:
        video_frame_filepaths += glob.glob(os.path.join(input_dirpath, ext))

    # Frame Rate Up-Conversion (if needed)
    if abs(in_fps - out_fps) < EPSILON:
        print("Input frame-rate is identical or very close to the output frame-rate, no conversion will be done.")
    else:
        FrameRateUpConverter(video_frame_filepaths,
                             in_fps=in_fps,
                             out_fps=out_fps,
                             output_frames_folder_path=output_dirpath,
                             num_procs=num_procs).up_convert_frame_rate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dirpath', help='Path to directory containing input frames')
    parser.add_argument('output_dirpath', help='Path to directory containing output frames')
    parser.add_argument('--in_fps', default=10, help='FPS for input frames')
    parser.add_argument('--out_fps', default=30, help='FPS for output frames')
    parser.add_argument('--num_procs', default=1, help='Number of parallel processes to use')
    args = parser.parse_args()

    run_frame_rate_conversion(args.input_dirpath, args.in_fps, args.out_fps, args.output_dirpath, args.num_procs)
